# Curriculum vitae

## Julie Bernier
525 rue Saint-Paul #401,
Québec, QC,
G1K 8N5,
418-929-2690

---

**Profil Professionnel:**
Expériences professionnelles dans plusieurs domaines demandant de travailler en équipe.
Motivée et dotée d'une solide expérience en gestion de projets et en gestion administrative.

**Compétences:**
Excellentes aptitudes pour la communication orale 
Esprit d'équipe 
Grande créativité
Maîtrise de logiciels: suite Office, Adobe Photoshop, Illustrator, Figma
Esprit d'initiative 

---

## Parcours professionnel

### Conseillère spécialisée   
Aliksir huiles essentielles
Québec, QC
, 2016 À 2020
- Conseillère en boutique; 
- Responsable de la formation des nouveaux employés;
- Écoute des besoins des clients pour identifier et recommander des produits appropriés; 
- Organiser la mise en place et la commercialisation de nouveaux produits sur des présentoirs visuellement attrayants et structurés pour une stimulation optimale des ventes; 
- Soutien de la gestion dans l'amélioration des opérations et la résolution des problèmes afin de fournir un service à la clientèle hors pair; 
- Préparations d'officines : calcul de recettes personnalisées et suivi avec les clients;
- Gestion des commandes, du matériels de bureaux et de l'inventaire de la succursale. 


## Membre travailleur   
### Bar-coop l'Agité(E), coopérative de solidarité
Québec, QC
, 2014 À 2015 
- Organisation et planification de spectacles et d'événements: promotion, 
gestion du budget et de la logistique; 
- Relation entre les artistes et la coopérative de travail; 
- Infographie /photographie; 
- Gestion des médias sociaux; 
- Service au bar; 
- Membre du conseil d'administration. 

## Propriétaire associée 
### Bar Octobre
Québec, QC
, 2009 À 2011 
- Gestion du personnel; 
- Organisation et planification de spectacles et d'événements: promotion, 
gestion du budget et de la logistique; 
- Gestion des médias sociaux; 
- Infographie; 
- Service au bar; 
- Supervision des opérations quotidiennes. 

## Propriétaire associée  
### Groupe Zone-Art
Québec, QC
, 2001 À 2009 
- Artiste peintre muraliste; 
- Recherche de subventions; 
- Direction des finances et organisation des budgets par projets; 
- Gestion d'inventaire; 
- S'assurer d'obtenir les documents, les autorisations, les certificats et les validations municipales, provinciales et fédérales. 

## Artiste Peintre Muraliste et Décoratrice
### Groupe Zone-Art
Québec, QC
, 2000 À 2002
- Recherches et documentation; 
- Dessins techniques et créations de murales et de faux-fini personnalisés; 
Évaluations et analyses de la clientèle afin d'entamer le processus de recherche de contrats; 
- Responsable de la création dans le cadre de projets de murales monumentales; 
- Maniement de l'équipement de peinture et de revêtement pour effectuer des travaux de haute qualité; 
- Maintenance de l'équipement de production pour une performance optimale et une plus grande durabilité. 
  
---

## Formations

**AEC en Developpement WEB**
•Cégep Garneau, formation continue
, Québec,QC
, 2020 à aujours'hui (formation en cours)

**Certificat en Aromathérapie scientifique**
•Centre de santé Chantal Lacroix
, Lévis, QC
, 2017-2018

**Animation 3D et synthèse d'images**
•Cégep de Limoilou
, Québec, QC
, 2008-2009

**Dessin technique spécialisé - Fresques murales et faux-fini**
•Atelier Hélène Fleury
, Québec, QC
, 2000-2002

**Arts plastiques**
•Cégep de Lévis-Lauzon
, Lévis, QC
, 1993-1995

---

## Langues parlées et écrites

Français 
Anglais 

---



